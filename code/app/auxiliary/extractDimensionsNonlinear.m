function [numStates,numInputs] = extractDimensionsNonlinear(handle)

    % Extract number of states
    u = zeros(1000,1);
    x = zeros(1000,1);
    
    f = handle(x,u);
    
    numStates = length(f);

    % Extract number of inputs
    x = zeros(numStates,1);
    
    terminate = 0;
    numInputs = 1;
    
    while ~terminate
        u = zeros(numInputs,1);    
        try
           handle(x,u);
           terminate = 1;
        catch
           numInputs = numInputs + 1;
        end
    end
        
        
end