function res = test_interval_isFullDim
% test_interval_isFullDim - unit test function of isFullDim
%
% Syntax:  
%    res = test_interval_isFullDim
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Mark Wetzlinger
% Written:      12-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% 1. Empty case
I = interval();

% compute dimension
isFullDimI = isFullDim(I);
true_result = false;
res_empty = isFullDimI == true_result;


% 2. Random cases
res_rand = true;
nrOfTests = 1000;

for i=1:nrOfTests

    % random dimension
    n = randi(50);

    % init random full-dimensional interval
    lb = -rand(n,1);
    ub = rand(n,1);
    I = interval(lb,ub);

    % check with correct solution
    if ~isFullDim(I)
        res_rand = false; break;
    end

    % init random lower-dimensional interval
    % ... by setting random dimension to 0
    randDim = randi(n);
    lb(randDim) = 0;
    ub(randDim) = 0;
    I = interval(lb,ub);

    % check with correct solution
    if isFullDim(I)
        res_rand = false; break;
    end

end

% combine results
res = res_empty && res_rand;

if res
    disp('test_isFullDim successful');
else
    disp('test_isFullDim failed');
end

%------------- END OF CODE --------------
