function res_empty = test_interval_interval
% test_interval_interval - unit test function of interval (constructor)
%
% Syntax:  
%    res = test_interval_interval
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Mark Wetzlinger
% Written:      20-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

tol = 1e-12;

% empty interval
I = interval();
res_empty = true;
if ~isempty(I)
    res_empty = false;
end

res_rand = true;
nrOfTests = 1000;
for i=1:nrOfTests

    % random dimension
    n = randi(25);
    
    % random lower bound, random upper bound
    a = -rand(n,1);
    b = rand(n,1);
    a_mat = -rand(n+1);
    b_mat = rand(n+1);
    
    % admissible initializations
    I = interval(a,b);
    if any(abs(I.inf - a) > tol) || any(abs(I.sup - b) > tol)
        res_rand = false; break;
    end
    
    I = interval(a);
    if any(abs(I.inf - a) > tol) || any(abs(I.sup - a) > tol)
        res_rand = false; break;
    end
    
    I = interval(a_mat);
    if any(any(abs(I.inf - a_mat) > tol)) || any(any(abs(I.sup - a_mat) > tol))
        res_rand = false; break;
    end
    
    I = interval(a_mat,b_mat);
    if any(any(abs(I.inf - a_mat) > tol)) || any(any(abs(I.sup - b_mat) > tol))
        res_rand = false; break;
    end
    
    % wrong initializations
    a_large = 1+rand(n,1);
    b_small = -1-rand(n,1);
    a_plus1 = -rand(n+1,1);
    b_plus1 = rand(n+1,1);
    
    % lower limit larger than upper limit
    try
        I = interval(a,b_small); % <- should throw error here
        res_rand = false; break;
    end
    try
        I = interval(a_large,b); % <- should throw error here
        res_rand = false; break;
    end
    
    % size of limits do not match
    try
        I = interval(a_plus1,b); % <- should throw error here
        res_rand = false; break;
    end
    try
        I = interval(a,b_plus1); % <- should throw error here
        res_rand = false; break;
    end
    try
        I = interval(a_mat,b); % <- should throw error here
        res_rand = false; break;
    end
    try
        I = interval(a,b_mat); % <- should throw error here
        res_rand = false; break;
    end
    
    % too many input arguments
    try
        I = interval(a,b,b); % <- should throw error here
        res_rand = false; break;
    end 
end


% combine results
res_empty = res_empty && res_rand;

if res_empty
    disp('test_interval successful');
else
    disp('test_interval failed');
end

%------------- END OF CODE --------------