function res = test_capsule_center
% test_capsule_center - unit test function of center
%
% Syntax:  
%    res = test_capsule_center
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Mark Wetzlinger
% Written:      28-August-2019
% Last update:  12-March-2021 (add empty case)
% Last revision:---

%------------- BEGIN CODE --------------

% empty capsule
C_empty = capsule();
res_empty = true;
if ~isempty(center(C_empty))
    res_empty = false;
end

% random capsules
res_rand = true;
for n=1:50
    
    % init capsule
    c_true = randn(n,1);
    C = capsule(c_true, randn(n,1), rand(1));

    % read center
    c = center(C);
    
    % check result
    if ~all(c == c_true)
        res_rand = false; break;
    end
end

% combine results
res = res_empty && res_rand;

if res
    disp('test_center successful');
else
    disp('test_center failed');
end

%------------- END OF CODE --------------