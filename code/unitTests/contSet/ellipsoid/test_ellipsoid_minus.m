function res = test_ellipsoid_minus
% test_ellipsoid_minus - unit test function of minus
%
% Syntax:  
%    res = test_ellipsoid_minus
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Ga�mann
% Written:      15-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
res = true;

% double
res = res && test_ellipsoid_minusDouble;

% ellipsoid
res = res && test_ellipsoid_minusEllipsoid;

if res
    disp('test_ellipsoid_minus successful');
else
    disp('test_ellipsoid_minus failed');
end
%------------- END OF CODE --------------
