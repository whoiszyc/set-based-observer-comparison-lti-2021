function res = test_ellipsoid_or
% test_ellipsoid_or - unit test function of or
%
% Syntax:  
%    res = test_ellipsoid_or
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Ga�mann
% Written:      14-October-2019
% Last update:  16-March-2021
% Last revision:---

%------------- BEGIN CODE --------------
res = true;

% double
res = res && test_ellipsoid_orDouble();

% ellipsoid
res = res && test_ellipsoid_orEllipsoid();


if res
    disp('test_ellipsoid_or successful');
else
    disp('test_ellipsoid_or failed');
end
%------------- END OF CODE --------------