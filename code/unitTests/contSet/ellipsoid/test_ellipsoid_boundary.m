function res = test_ellipsoid_boundary
% test_ellipsoid_boundary - unit test function of boundary
%
% Syntax:  
%    res = test_ellipsoid_boundary
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Ga�mann
% Written:      19-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

runs = 10;
res = true;
bools = [false,true];
for i=1:5:20
    for j=1:runs
        N = 10*i;
        for k=1:2
            E = ellipsoid.generateRandom(i,bools(k));
            B = boundary(E,N);
            [b,Val] = containsPoint(E,B);
            if ~all(b) || ~all(abs(1-Val)<=E.TOL)
                res = false;
                break;
            end
        end
        if ~res
            break;
        end
    end
    if ~res
        break;
    end
end
if ~res
    disp('test_ellipsoid_boundary failed');
else
    disp('test_ellipsoid_boundary successful');
end

%------------- END OF CODE --------------