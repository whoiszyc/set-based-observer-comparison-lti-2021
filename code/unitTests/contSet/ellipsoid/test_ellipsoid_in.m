function res = test_ellipsoid_in
% test_ellipsoid_in - unit test function of in
%
% Syntax:  
%    res = test_ellipsoid_in
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Ga�mann
% Written:      15-October-2019
% Last update:  07-August-2020
%               19-March-2021
% Last revision:---

%------------- BEGIN CODE --------------
res = true;

% check double 
[~,r] = evalc('test_ellipsoid_containsPoint');
res = res && r;

% check ellipsoid 
res = res && test_ellipsoid_inEllipsoid;

% check zonotope
res = res && test_ellipsoid_inZonotope;

if res
    disp('test_ellipsoid_in successful');
else
    disp('test_ellipsoid_in failed');
end
%------------- END OF CODE --------------