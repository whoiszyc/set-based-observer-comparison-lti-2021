function res = test_ellipsoid_containsPoint
% test_ellipsoid_containsPoint - unit test function of containsPoint
%
% Syntax:  
%    res = test_ellipsoid_containsPoint
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Ga�mann, Matthias Althoff
% Written:      13-March-2019
% Last update:  02-Sep-2019 (rename containsPoint)
%               16-March-2021 (removed sample dependency)
% Last revision:---

%------------- BEGIN CODE --------------

runs = 10;
res = true;
bools = [false,true];
for i=1:5:20
    for j=1:runs
        N = 10*i;
        for k=1:2
            E = ellipsoid.generateRandom(i,bools(k));
            samples = randPoint(E,N);
            if ~all(containsPoint(E,samples))
                res = false;
                break;
            end
        end
        if ~res
            break;
        end
    end
    if ~res
        break;
    end
end
if ~res
    disp('test_ellipsoid_containsPoint failed');
else
    disp('test_ellipsoid_containsPoint successful');
end

%------------- END OF CODE --------------