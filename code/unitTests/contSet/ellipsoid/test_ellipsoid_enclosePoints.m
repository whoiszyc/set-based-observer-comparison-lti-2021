function res = test_ellipsoid_enclosePoints
% test_ellipsoid_randPoint - unit test function of test_ellipsoid_randPoint
%
% Syntax:  
%    res = test_ellipsoid_randPoint
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Ga�mann
% Written:      18-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

runs = 5;
res = true;
bools = [false,true];
for i=5:5:10
    for j=1:runs
        N = 10*i;
        for k=1:2
            % generate point cloud
            V = randn(i,N);
            % generate non-degenerate and degenerate point clouds
            for m=1:2
                [Q,~] = qr(V);
                V = Q'*V;
                r = ceil(i*rand);
                % for bools(m)=false, generate degenerate point cloud
                V(r,:) = bools(m)*V(r,:);
                V = Q*V;
                % test both methods
                E_cov = ellipsoid.enclosePoints(V,'cov');
                E_mv = ellipsoid.enclosePoints(V,'min-vol');
                if ~all(containsPoint(E_cov,V)) || ~all(containsPoint(E_mv,V))
                    res = false;
                    break;
                end
            end
            if ~res
                break;
            end
        end
        if ~res
            break;
        end
    end
    if ~res
        break;
    end
end
if ~res
    disp('test_ellipsoid_enclosePoints failed');
else
    disp('test_ellipsoid_enclosePoints successful');
end

%------------- END OF CODE --------------