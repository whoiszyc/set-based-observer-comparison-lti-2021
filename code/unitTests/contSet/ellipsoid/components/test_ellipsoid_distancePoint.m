function res = test_ellipsoid_distanceDouble
% test_ellipsoid_distanceDouble - unit test function of test_ellipsoid_distanceDouble
%
% Syntax:  
%    res = test_ellipsoid_distanceDouble
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Gaßmann
% Written:      18-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
res = true;
nRuns = 2;
bools = [false,true];
% smaller dimensions since vertices and halfspaces are involved
for i=5:5:10
    for j=1:nRuns
        for k=1:2 
            E = ellipsoid.generateRandom(i,bools(k));
            N = 2*i;
            % generate random point cloud center around origin
            V = randn(i,N);
            for m=1:2
                [Q,~] = qr(V);
                V = Q'*V;
                r = ceil(i*rand);
                % for bools(m)=false, generate degenerate point cloud
                V(r,:) = bools(m)*V(r,:);
                V = Q*V;
                V_c = mat2cell(V,E.dim,ones(1,N));
                D = distance(E,V_c);
                q_abs = max(abs(E.q));
                ind_d = D/q_abs<=E.TOL;
                ind_c = containsPoint(E,V);
                % check if masks match
                if ~all(ind_c==ind_d)
                    res = false;
                    break;
                end
            end
            if ~res
                break;
            end
        end
        if ~res
            break;
        end
    end
    if ~res
        break;
    end
end
%------------- END OF CODE --------------