function res = test_ellipsoid_andHalfspace
% test_ellipsoid_andHalfspace - unit test function of test_ellipsoid_andHalfspace
%
% Syntax:  
%    res = test_ellipsoid_andHalfspace
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Gaßmann
% Written:      17-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
res = true;
nRuns = 2;
bools = [false,true];
for i=10:5:15
    for j=1:nRuns
        for k=1:2
            E = ellipsoid.generateRandom(bools(k),i);
            % generate tangent
            c = randn(E.dim,1);
            c = c/norm(c);
            [val,x] = supportFunc(E,c);
            % completely contains E
            h1 = halfspace(c,val);
            if ~((E&h1)==E)
                res = false;
                break;
            end
            % completely outside (ignoring touching)
            h2 = halfspace(-c,-val);
            res2 = E&h2;
            q_abs = max(res2.q);
            if q_abs == 0
                q_abs = 1;
            end
            if 1/cond(res2.Q)>E.TOL || any(abs((res2.q-x)/q_abs)>E.TOL)
                res = false;
                break;
            end
            % choose random point within E
            N = 2*dim(E);
            B = boundary(E,N);
            m = ceil(N*rand);
            s = E.q + rand*(B(:,m)-E.q);
            v = randn(dim(E),1);
            h3 = halfspace(v,v'*s);
            if isempty(E&h3)
                res = false;
                break;
            end
        end
        if ~res
            break;
        end
    end
    if ~res
        break;
    end
end
%------------- END OF CODE --------------