function res = test_ellipsoid_orEllipsoidIA
% test_ellipsoid_orEllipsoidIA - unit test function of test_ellipsoid_orEllipsoidIA
%
% Syntax:  
%    res = test_ellipsoid_orEllipsoidIA
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Gaßmann
% Written:      17-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
res = true;
nRuns = 2;
bools = [false,true];
for i=10:5:15
    for j=1:nRuns
        for k=1:2
            E1 = ellipsoid.generateRandom(i,false);
            E2 = ellipsoid.generateRandom(i,false);
            E3 = ellipsoid.generateRandom(i,bools(k)); 
            % compute outer approx (check overloaded syntax)
            Eo1 = E1|E2;
            % check regular syntax
            Eo2 = or(E1,{E2,E3},'o');
            % compute inner approx
            Ei1 = or(E1,E2,'i');
            Ei2 = or(E1,{E2,E3},'i');
            % check if inner contained in outer
            if ~in(Eo1,Ei1) || ~in(Eo2,Ei2)
                res = false;
                break;
            end
            Y1 = boundary(E1,i);
            Y2 = boundary(E2,i);
            Y3 = boundary(E3,i);
            if ~all(containsPoint(Eo1,Y1)) || ~all(containsPoint(Eo1,Y2)) ||...
               ~all(containsPoint(Eo2,Y1)) || ~all(containsPoint(Eo2,Y2)) ||...
               ~all(containsPoint(Eo2,Y3))
                res = false;
                break;
            end
        end
        if ~res
            break;
        end
    end
    if ~res
        break;
    end
end
%------------- END OF CODE --------------