function res = test_ellipsoid_inEllipsoid
% test_ellipsoid_inEllipsoid - unit test function of inEllipsoid
%
% Syntax:  
%    res = test_ellipsoid_inEllipsoid
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Gaßmann
% Written:      16-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
res = true;
nRuns = 2;
for i=10:5:15
    for j=1:nRuns
        E1 = ellipsoid.generateRandom(i,true);
        E2 = ellipsoid.generateRandom(i,false);
        % check whether E1 dg, E2 non-d results in E2\subseteq E1 = false
        if in(E1,E2)
            res = false;
            break;
        end
        % E3 contains E2
        E3 = ellipsoid(0.9*E2.Q,E2.q+0.9*E2.TOL*(2*rand(E2.dim,1)-1));
        % check if E3 contains E2
        if ~in(E3,E2)
            res = false;
            break;
        end
    end
    if ~res
        break;
    end
end
%------------- END OF CODE --------------