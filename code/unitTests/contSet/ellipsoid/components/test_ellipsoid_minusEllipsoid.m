function res = test_ellipsoid_orEllipsoidIA
% test_ellipsoid_orEllipsoidIA - unit test function of test_ellipsoid_orEllipsoidIA
%
% Syntax:  
%    res = test_ellipsoid_orEllipsoidIA
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Gaßmann
% Written:      17-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
res = true;
nRuns = 2;
for i=10:5:15
    for j=1:nRuns
        % for outer approx, both need to be degenerate, otherwise "and"
        % will throw error 
        E1 = ellipsoid.generateRandom(i,false);
        E2 = ellipsoid.generateRandom(i,false);
        % test "direct syntax
        E1o = E1-E2;
        E1i = minus(E1,E2,'i');
        [U,S,~] = svd(E2.Q);
        m = ceil(dim(E2)*rand);
        S(m,m) = 0;
        % generate degenerate ellipsoid contained in E2
        E3 = ellipsoid(U*S*U',E2.q);
        E2i = minus(E1,E3,'i');
        if isempty(E1o) 
            if ~(E1==E2) && in(E1,E2)
                res = false;
                break;
            end
            continue;
        end
        if ~in(E1o,E1i) || ~in(E1o,E2i)
            res = false;
            break;
        end
        E3i = minus(E2,E3,'i');
        if isempty(E3i)
            res = false;
            break;
        end
        % test cell array syntax
        E2o = minus(E1,{E2});
        if ~(E1o==E2o)
            res = false;
            break;
        end
        % check points
        Y1 = boundary(E1,nPoints);
        Y2 = boundary(E2,nPoints);
        Y  = sumPoints(Y1,-Y2);
        %check if Y \in Eo
        if ~all(containsPoint(E1o,Y))
            res = false;
            break;
        end
    end
    if ~res
        break;
    end
end
%------------- END OF CODE --------------