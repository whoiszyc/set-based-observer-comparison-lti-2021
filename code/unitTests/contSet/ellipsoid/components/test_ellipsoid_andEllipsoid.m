function res = test_ellipsoid_andEllipsoid
% test_ellipsoid_andEllipsoid - unit test function of test_ellipsoid_andEllipsoid
%
% Syntax:  
%    res = test_ellipsoid_andEllipsoid
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Gaßmann
% Written:      17-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
res = true;
nRuns = 2;
bools = [false,true];
for i=10:5:15
    for j=1:nRuns
        for k=1:2
            E1 = ellipsoid.generateRandom(false,i);
            N = 10*i;
            b = boundary(ellipsoid(E1.Q),N);
            % use boundary points as well as interior points
            s = rand(1,N).*b;
            Y = E1.q + [b,s];
            % use point from Y as center to make sure intersection is not
            % empty
            E2 = ellipsoid.generateRandom(bools(k),i);
            m = ceil(N*rand);
            E2 = ellipsoid(E2.Q,Y(:,m));
            Eo = E1&E2;
            if isempty(Eo)
                res = false;
                break;
            end

            %see which parts of Y are both E1 and E2
            res1 = containsPoint(E1,Y);
            res2 = containsPoint(E2,Y);
            %find points which are in the intersection
            R = res1 & res2;
            Y = Y(:,R);
            %check if Y points are in E
            if ~all(containsPoint(Eo,Y))
                res = false;
                break;
            end
            
            % only works if E1 and E2 full-dimensional, otherwise Eo and Ei
            % are both degenerate
            if ~bools(k)
                Ei = and(E1,E2,'i');
                if ~in(Eo,Ei)
                    res = false;
                    break;
                end
            end
        end
        if ~res
            break;
        end
    end
    if ~res
        break;
    end
end
%------------- END OF CODE --------------