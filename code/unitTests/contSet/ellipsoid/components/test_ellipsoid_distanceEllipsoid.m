function res = test_ellipsoid_distanceEllipsoid
% test_ellipsoid_distanceEllipsoid - unit test function of test_ellipsoid_distanceEllipsoid
%
% Syntax:  
%    res = test_ellipsoid_distanceEllipsoid
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Gaßmann
% Written:      18-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
res = true;
nRuns = 2;
bools = [false,true];
for i=5:5:10
    for j=1:nRuns
        for k=1:2 
            E1 = ellipsoid.generateRandom(i,false);
            E2 = ellipsoid.generateRandom(i,bools(k));
            N = 2*i;
            m = ceil(N*rand);
            B = boundary(E1,N);
            b = B(:,m);
            s = E1.q + rand*(b-E1.q);
            % E1 and E2 are intersecting
            E2 = ellipsoid(E2.Q,s);
            q_abs = max(abs([E1.q;E2.q]));
            if distance(E1,E2)/q_abs>E1.TOL
                res = false;
                break;
            end
            IntE1 = interval(E1);
            q3 = E1.q+2.1*rad(IntE1);
            [U,S,~] = svd(E1.Q);
            r = ceil(i*rand);
            S(r,r) = bools(k)*S(r,r);
            % guaranteed to not intersect with E1
            E3 = ellipsoid(U*S*U',q3);
            if distance(E1,E3)/q_abs<=E1.TOL
                res = false;
                break;
            end
        end
        if ~res
            break;
        end
    end
    if ~res
        break;
    end
end
%------------- END OF CODE --------------