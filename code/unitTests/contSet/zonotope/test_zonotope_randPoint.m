function res = test_zonotope_randPoint
% test_zonotope_randPoint - unit test function of randPoint
%
% Syntax:  
%    res = test_zonotope_randPoint
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Mark Wetzlinger
% Written:      17-Sep-2019
% Last update:  14-March-2021 (MW, adapt to new syntax)
% Last revision:---

%------------- BEGIN CODE --------------

res = true;
tol = 1e-9;

% check empty conZonotope object -> error
Z = zonotope();
try 
    p = randPoint(Z);
catch ME
    if ~strcmp(ME.identifier,'CORA:emptySet')
        res = false;
    end
end


% number of tests
nrOfTests = 100;

for i=1:nrOfTests
    % random dimension
    n = randi([2,8]); % small because of containment checks
    
    % random center
    c = randn(n,1);
    % random generator matrix -> parallelotope
    G = randn(n);
    
    % instantiate zonotope
    Z = zonotope(c,G);
    
    % compute random points
    nrPts = 10;
    pNormal = randPoint(Z,nrPts,'normal');
    pExtreme = randPoint(Z,nrPts,'extreme');
    
    % check for containment in zonotope
    % (use containsPoint instead of in, since the latter has a bug)
    for j=1:nrPts
        if ~containsPoint(Z,pNormal(:,j))
            res = false; break;
        elseif ~containsPoint(enlarge(Z,1+tol),pExtreme(:,j))
            % enlarging Z is required, otherwise wrong result!
            res = false; break;
        end
    end
    
    if ~res
        break;
    end
end


if res
    disp('test_zonotope_randPoint successful');
else
    disp('test_zonotope_randPoint failed');
end

%------------- END OF CODE --------------
