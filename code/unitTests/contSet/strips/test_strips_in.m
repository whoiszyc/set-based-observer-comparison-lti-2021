function res = test_strips_in
% test_strips_in - unit test function of in for strips
%
% Syntax:  
%    res = test_strips_in
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Matthias Althoff
% Written:      08-Mar-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------


%% create possible states of the uncertain measurement function
% y = Cx + V
% where V is a zonotope

% measurement uncertainty
V{1} = zonotope([0, 1]);
V{2} = zonotope([0, -3, -2, -1; 0, 2, 3, 4]);

% measurement matrix
C{1} = [-2 1];
C{2} = [-2, 1; 1, 1];

% measurements
y{1} = 1;
y{2} = [1; 1];

% bound possible states
P_bound = mptPolytope(zonotope([zeros(2,1),10*eye(2)]));

% init partial results
resPartial = [];

%% loop over examples
for iExample = 1:2

    % width of strips
    d = supremum(abs(interval(V{iExample})));

    % create strips
    S = strips(C{iExample},d,y{iExample});
    
    % bound possible values
    S_bound = S & P_bound;
    
    % slightly scale S_bound
    c = center(S_bound);
    S_bound = (1-1e-8)*(S_bound - c) + c;

    %% check individual points
    % create random points
    pRand_1 = randPoint(S_bound,10,'normal');
    pRand_2 = randPoint(S_bound,10,'extreme');
    pRand = [pRand_1, pRand_2];
    
    % check if all points are in the strip
    for iPoint = 1:length(pRand(1,:))
        resPartial(end+1) = in(S,pRand(:,iPoint));
    end
    
%     figure 
%     hold on
%     plot(pRand(1,:),pRand(2,:),'.k','MarkerSize',10);
%     plot(S,[1,2],'r');
    
    %% check polytope
    % create polytope from points
    P = mptPolytope(pRand');

    % check if polytope is in the strip
    resPartial(end+1) = in(S,P);
end 

% check result
res = all(resPartial);

if res
    disp('test_in successful');
else
    disp('test_in failed');
end

%------------- END OF CODE --------------
