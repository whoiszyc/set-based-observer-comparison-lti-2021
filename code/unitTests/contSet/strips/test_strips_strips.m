function res = test_strips_strips
% test_strips_strips - unit test function of strips (constructor)
%
% Syntax:  
%    res = test_strips_strips
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Mark Wetzlinger
% Written:      21-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

tol = 1e-12;

% empty strip
S = strips();
res_empty = true;
if ~isempty(S)
    res_empty = false;
end

res_rand = true;
nrOfTests = 1000;
for i=1:nrOfTests

    % random dimension
    n = randi(25);
    % number of strips
    nrStrips = randi(10);
    
    % random normal vector, bounds, and offset
    C = randn(nrStrips,n);
    d = randn(nrStrips,1);
    y = randn(nrStrips,1);
    
    % admissible initializations
    % C and d
    S = strips(C,d);
    if any(any(abs(S.C - C) > tol)) || any(abs(S.d - d) > tol)
        res_rand = false; break;
    end

    % C, d, and y
    S = strips(C,d,y);
    if any(any(abs(S.C - C) > tol)) || any(abs(S.d - d) > tol) ...
            || any(abs(S.y - y) > tol)
        res_rand = false; break;
    end    
    
    % wrong initializations
    C_plus1 = randn(nrStrips+1,n);
    d_plus1 = randn(nrStrips+1,1);
    y_plus1 = randn(nrStrips+1,1);
    
    % empty input arguments
    try
        S = strips(C,[]); % <- should throw error here
        res_rand = false; break;
    end
    try
        S = strips(C,[],y); % <- should throw error here
        res_rand = false; break;
    end
    try
        S = strips([],d); % <- should throw error here
        res_rand = false; break;
    end
    try
        S = strips([],d,y); % <- should throw error here
        res_rand = false; break;
    end
    
    % C and d do not match
    try
        S = strips(C_plus1,d); % <- should throw error here
        res_rand = false; break;
    end
    try
        S = strips(C_plus1,d,y); % <- should throw error here
        res_rand = false; break;
    end
    try
        S = strips(C,d_plus1); % <- should throw error here
        res_rand = false; break;
    end
    try
        S = strips(C,d_plus1,y); % <- should throw error here
        res_rand = false; break;
    end
    
    % C and y do not match
    try
        S = strips(C_plus1,d,y); % <- should throw error here
        res_rand = false; break;
    end
    try
        S = strips(C,d,y_plus1); % <- should throw error here
        res_rand = false; break;
    end
    
    % too many input arguments
    try
        S = strips(C,d,y,y); % <- should throw error here
        res_rand = false; break;
    end 
end


% combine results
res = res_empty && res_rand;

if res
    disp('test_strips successful');
else
    disp('test_strips failed');
end

%------------- END OF CODE --------------