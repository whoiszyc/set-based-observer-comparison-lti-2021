function res = test_linearSysDT_observe_02_reactor
% test_linearSysDT_observe_02_reactor - unit test function for guaranteed
% state estimation of linear discrete-time systems.
%
% Checks the solution of the linearSysDT class for a reactor example 
% from Sec. 7.1 of [1]; It is checked whether the enclosing interval of the 
% final observed set is close to an interval provided by a previous 
% solution that has been saved
%
% Syntax:  
%    res = test_linearSysDT_observe_02_reactor()
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Reference:
%    [1] T. Alamo, J. M. Bravo, and E. F. Camacho. Guaranteed
%        state estimation by zonotopes. Automatica, 41(6):1035–1043, 2005.
%
% Example: 
%     -
 
% Author:       Matthias Althoff
% Written:      19-November-2020
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% Parameters --------------------------------------------------------------

params.tStart = 0; %start time --> remove after fixing options checks
params.tFinal = 20; %final time
params.R0 = zonotope(zeros(2,1),3*eye(2)); %initial set
params.U = []; % input set
params.V = 0.2*zonotope([0,1]); % sensor noise set
params.W = 0.02*[-6; 1]*zonotope([0,1]); % disturbance set
params.uTrans = zeros(2,1); %input transition --> remove after fixing options checks
params.uTransVec = zeros(2,1e3); %input transition --> remove after fixing options checks

% Reachability Settings ---------------------------------------------------

options.zonotopeOrder = 20; %zonotope order
options.timeStep = 1;

% Simulation Settings -----------------------------------------------------

options.points = 1;
options.p_conf = 0.999; % probability that sample of normal distribution within specified set

% System Dynamics ---------------------------------------------------------

% system matrices (we select delta = 1)
A = [0 -0.5;...
    1 1];
B = 1;
c = zeros(2,1);
C = [-2 1];

reactor = linearSysDT('reactor',A, B, c, C, options.timeStep); %initialize system

% Set of evaluated estimators
Estimator = {
    'VolumeMinI' % works; not yet validated
    %'VolumeMinII' % works; not yet validated
    %'SegMin' % works; not yet validated
    %'FRadiusMin' % to be implemented
    %'PRadiusA' % works; not yet validated
    %'PRadiusB' % works; not yet validated
    %'PRadiusC' % works; not yet validated
    %'PRadiusD' % does not yet work
    %'PRadiusDII' % does not yet work
    %'FRadiusMin_IOA' % works; not yet validated
    %'PRadiusA_IOA' % works; not yet validated
    %'NominalGain' % works; not yet validated
    'HinfGain' % works; not yet validated
    'PRadiusA_ESO'
    'Kalman16_C_ESO'
    'Kalman96_A_ESO'
    'Kalman96_B_ESO'
    'HinfGain_1_ESO'
    'HinfGain_2_ESO'
    };

% simulate result assuming Gaussian distributions
simRes = simulateGaussian(reactor, params, options);

% obtain output values
for i=1:length(simRes.t{1})
    % create measurement noise
    v = randPointGaussian(params.V,options.p_conf);
    % obtain output value
    params.yVec(:,i) = C*simRes.x{1}(i,:)' + v;
end

 
% loop over estimators
for iEst = 1:length(Estimator)

    % set algorithm
    estName = Estimator{iEst};
    options.alg = estName;

    %% Initial sets, disturbance sets, and noise sets
    if any(strcmp(estName,{'PRadiusA_ESO','Kalman16_C_ESO','Kalman96_A_ESO',...
            'Kalman96_B_ESO','HinfGain_1_ESO','HinfGain_2_ESO'}))
        % ellipsoids
        params.R0 = ellipsoid(eye(size(sys.A,1)),zeros(size(sys.A,1),1)); % Initial State bounded in unity box
        params.W =  ellipsoid(eye(size(sys.E,1)),zeros(size(sys.E,1),1)); 
        params.V =  ellipsoid(eye(size(sys.C,1)),zeros(size(sys.C,1),1));
    end

    % observe
    EstSet{iEst} = observe(reactor,params,options);
    
    % plot results
    for iDim = 1:2
        figure; hold on;
        % plot time elapse
        plotOverTime(EstSet{iEst},iDim,'FaceColor',[.6 .6 .6],'EdgeColor','none');
        % plot simulation
        plotOverTime(simRes,iDim);

        % label plot
        xlabel('t');
        ylabel(['x_{',num2str(iDim),'}']);
    end

end
        
%------------- END OF CODE --------------
