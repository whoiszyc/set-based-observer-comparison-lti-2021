function createTableObservers(obj,params,options)
% createTableObservers - creates table for the performance of set-based observers
%
% Syntax:  
%    createTableObservers()
%
% Inputs:
%    -
%
% Outputs:
%    -
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none
%
% References: 
%   -

% Author:       Matthias Althoff
% Written:      18-Mar-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% set path
path = [coraroot '/unitTests/contDynamics/linearSysDT/results'];
close all

% load file
name = 'Vehicle_1_States_ZOrder_100_20-Mar-2021_a';
load(name);

% set format
formatSpec = '%#4.4g';

%% absolute values
% create file for table
fid = fopen([path filesep 'table_' name '_absolute.m'],'w');

% loop through observers
for iObserver = 1:length(estSet)
    % currentObserver
    currObserver = estSet{iObserver};
    % write name
    fprintf(fid, '%-13s', [currObserver.Name,' & ']);
    % write type
    fprintf(fid, '%-17s', [determineType(currObserver),' & ']);
    % add ready for control option
    fprintf(fid, '%-15s', '\xmark/\cmark & ');
    % add radii
    radVec = currObserver.Performance.IRadius;
    for iRad = 1:length(radVec)
        fprintf(fid, '%s', [num2str(radVec(iRad), formatSpec),' & ']);
    end
    % add average
    fprintf(fid, '%s', [num2str(mean(radVec), formatSpec),' & ']);
    % print computation time in ms
    fprintf(fid, '%s\n', [num2str(1e3*currObserver.tIteration, formatSpec),' \\']);
    % collect values for relative table
    radMatrix(iObserver,:) = radVec;
end

%close file
fclose(fid);


%% relative values
% create file for table
fid = fopen([path filesep 'table_' name '_relative.m'],'w');

% find best radius for each dimension
bestRad = min(radMatrix);

% rompute relative values for radMatrix
for iCol = 1:length(radMatrix(1,:))
    radMatrixRel(:,iCol) = radMatrix(:,iCol)/bestRad(iCol);
end

% loop through observers
for iObserver = 1:length(estSet)
    % currentObserver
    currObserver = estSet{iObserver};
    % write name
    fprintf(fid, '%-13s', [currObserver.Name,' & ']);
    % write type
    fprintf(fid, '%-17s', [determineType(currObserver),' & ']);
    % add ready for control option
    fprintf(fid, '%-15s', '\xmark/\cmark & ');
    % add radii
    radVecRel = radMatrixRel(iObserver,:);
    for iRad = 1:length(radVecRel)
        fprintf(fid, '%s', [num2str(radVecRel(iRad), formatSpec),' & ']);
    end
    % add average
    fprintf(fid, '%s', [num2str(mean(radVecRel), formatSpec),' & ']);
    % print computation time in ms
    fprintf(fid, '%s\n', [num2str(1e3*currObserver.tIteration, formatSpec),' \\']);
end
%% last row
% new columns
fprintf(fid, '%-13s', ' & & & ');
% best values
for iRad = 1:length(bestRad)
    fprintf(fid, '%s', [num2str(bestRad(iRad), formatSpec),' & ']);
end


%close file
fclose(fid);


end

% determine type of observer
function type = determineType(observer)
    % obtain first set
    firstSet = observer.EstStates.timePoint.set{1};
    % zonotope?
    if isa(firstSet,'zonotope')
        type = 'zonotope';
    elseif isa(firstSet,'ellipsoid')
        type = 'ellipsoid';
    elseif isa(firstSet,'conZonotope')
        type = 'constr. zono.';
    end
        
end