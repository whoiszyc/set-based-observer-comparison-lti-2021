function [paramsList,optionsList] = config_contDynamics_simulateRandom(sys,params,options)
% config_contDynamics_simulateRandom - configuration file for validation of
%    model parameters and algorithm parameters
%
% Syntax:
%    [paramsList,optionsList] = config_contDynamics_simulateRandom(sys,params,options)
%
% Inputs:
%    sys - linParamSys object
%    params - user-defined model parameters
%    options - user-defined algorithm parameters
%
% Outputs:
%    paramsList - list of model parameters
%    optionsList - list of algorithm parameters
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Mark Wetzlinger
% Written:      03-February-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% 1. init lists
initParamsOptionsLists();

% append entries to list of model parameters
add2params('R0','mandatory',{@(val)any(ismember(getMembers('R0'),class(val))),@(val)eq(dim(val),sys.dim)});
add2params('U','default',{@(val)any(ismember(getMembers('U'),class(val)))});
add2params('u','default',{@isnumeric});
add2params('tStart','default',{@isscalar,@(val)ge(val,0)});
add2params('tFinal','mandatory',{@isscalar,@(val)ge(val,params.tStart)});
add2params('paramInt','mandatory',{@(val)length(val)==sys.nrOfParam,...
    @(val)isa(val,'interval') || (isvector(val) && isnumeric(val))},{@()isa(sys,'nonlinParamSys')});
add2params('y0guess','mandatory',{@(val)length(val)==sys.nrOfConstraints},{@()isa(sys,'nonlinDASys')});

% append entries to list of algorithm parameters
add2options('points','mandatory',{@isscalar,@isnumeric,@(val)mod(val,1)==0,@(val)ge(val,1)});
add2options('fracVert','mandatory',{@isscalar,@isnumeric,@(val)ge(val,0),@(val)le(val,1)});
add2options('fracInpVert','mandatory',{@isscalar,@isnumeric,@(val)ge(val,0),@(val)le(val,1)});
add2options('inpChanges','mandatory',{@isscalar,@isnumeric,@(val)mod(val,1)==0,@(val)ge(val,0)});

% 3. prepare lists for output args
[paramsList,optionsList] = outputParamsOptionsLists();

end

%------------- END OF CODE --------------
