function res = c_pHA_startLoc(val,sys,params)
% c_pHA_startLoc - costum validation function for params.startLoc
%
% Syntax:
%    res = c_pHA_startLoc(val,sys,options)
%
% Inputs:
%    val - value for given param / option
%    sys - parallelHybridAutomaton object
%    params - model parameters
%
% Outputs:
%    res - logical whether validation was successful
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none
%
% References: 
%   -

% Author:       Mark Wetzlinger
% Written:      04-Februar-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% assume check ok
res = true;

numComp = length(sys.components);

if ~all(size(params.startLoc) == [numComp,1])
    res = false; return;
else
    for c=1:numComp
        % loop through every component
        comp = sys.components{c};
        numLoc = length(comp.location);
        if params.startLoc(c) > numLoc
            res = false; return;
        elseif params.startLoc(c) <= 0
            res = false; return;
        elseif mod(params.startLoc(c),1.0) ~= 0
            res = false; return;
        end
    end
end


end

%------------- END OF CODE --------------
