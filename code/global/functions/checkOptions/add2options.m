function add2options(name,status,checkfuncs,varargin)
%ADD2OPTIONS Summary of this function goes here
%   Detailed explanation goes here

global fullOptionsList;

if isempty(varargin)
    fullOptionsList = add2list(fullOptionsList,name,status,checkfuncs);
else
    fullOptionsList = add2list(fullOptionsList,name,status,checkfuncs,varargin{1});
end

end