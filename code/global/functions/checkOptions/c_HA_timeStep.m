function res = c_HA_timeStep(val,sys,options)
% c_HA_timeStep - costum validation function for options.timeSteps
%
% Syntax:
%    res = c_HA_timeStep(val,sys,options)
%
% Inputs:
%    val - value for given param / option
%    sys - hybridAutomaton object
%    options - algorithm parameters
%
% Outputs:
%    res - logical whether validation was successful
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none
%
% References: 
%   -

% Author:       Mark Wetzlinger
% Written:      04-Februar-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% assume check ok
res = true;

if ~iscell(val)
    % single value given (for all locations)
    if ~isscalar(val)
        res = false; return;
    elseif ~isnumeric(val)
        res = false; return;
    elseif val <= 0
        res = false; return;
    end
    
else
    numLoc = length(sys.location);
    % cell array, different timeStep for each location
    if ~all(size(val) == [numLoc,1]) && ~all(size(val) == [1,numLoc])
        res = false; return;
    else
        for i=1:numLoc
            if ~isscalar(val{i})
                res = false; return;
            elseif ~isnumeric(val{i})
                res = false; return;
            elseif val{i} <= 0
                res = false; return;
            end
        end
    end 
        
end


end

%------------- END OF CODE --------------
