function P = sumPoints(varargin)
% ndimCross - Computes the n-dimensional cross product
%
% Syntax:  
%    [v] = nDimCross(Q)
%
% Inputs:
%    Q - 
%
% Outputs:
%    v - 
%
% Example: 
%    -
%
% Other m-files required: vertices, polytope
% Subfunctions: none
% MAT-files required: none
%
% See also: interval,  vertices

% Author:       Matthias Althoff
% Written:      14-September-2006 
% Last update:  22-March-2007
% Last revision: ---

%------------- BEGIN CODE --------------
if isempty(varargin)
    error('No input arguments provided!');
end

% check if all are doubles
if ~all(cellfun(@(inp)isa(inp,'double'),varargin))
    error('Some inputs are not of type double!');
end

% check if all inputs have same row-dimension
if length(unique(cellfun(@(inp) size(inp,1),varargin)))~=1
    error('Not all inputs have same row dimension!');
end

V = combvec(varargin{:});
N = length(varargin);
n = size(varargin{1},1);
P = zeros(n,size(V,2));
for i=1:n
    P(i,:) = sum(V(i:n:N*n,:),1);
end
P = unique(P','stable','rows')';
%------------- END OF CODE --------------