% system dynamics
f = @(x,u) [1-2*x(1) + 3/2 * x(1)^2*x(2); ...
            x(1)-3/2*x(1)^2*x(2)];
    
sys = nonlinearSys(f); 

% parameter
params.tFinal = 1;
params.R0 = interval([0.75;0],[1;0.25]);

% reachability settings
options.algInner = 'scale';
options.timeStep = 0.001;                           
options.taylorTerms = 10;                            
options.zonotopeOrder = 50;       
options.intermediateOrder = 20;
options.errorOrder = 10;

% reachability analysis
[Rin,Rout] = reachInner(sys,params,options);

% visualization
figure; hold on;
plot(Rout.timePoint.set{end},[1,2],'b');
plot(Rin.timePoint.set{end},[1,2],'r');
box on
axis equal
xlim([0.6,0.8])
ylim([0.45,0.65])
set(gcf, 'Units', 'centimeters', 'Position', [4, 4, 6, 6]);
yticks([0.5 0.6]);
xlabel('$x_1$','interpreter','latex','FontSize',15)
ylabel('$x_2$','interpreter','latex','FontSize',15)