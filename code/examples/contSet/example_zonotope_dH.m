function res = example_zonotope_dH
% example_zonotope_dH - test function for different over-approximations
%   of the Hausdorff distance between a zonotope and its
%   box over-approximation
%
% Syntax:  
%    res = example_zonotope_dH
%
% Inputs:
%    -
%
% Outputs:
%    res - logical
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none
%
% Author:        Mark Wetzlinger
% Written:       08-March-2021
% Last update:   ---
% Last revision: ---

%------------- BEGIN CODE --------------

% settings
testsPerDim = 100;
dims = 2:2:8;
gamma = 100;

% loop over different dimensions
for i=1:length(dims)
    
    % current dimension
    n = dims(i);
    gamma = n;
    
    % loop over number of tests
    for j=1:testsPerDim

        % generator matrix
        G = -6 + randi(11,n,gamma); % [-5,5]
        idxZeros = ~any(G,1);
        % prevent zeros(n,1)-generators
        while any(idxZeros)
            gamma_new = nnz(idxZeros);
            G(:,idxZeros) = [];
            G_new = -5 + randi(10,n,gamma_new);
            G = [G, G_new];
            idxZeros = ~any(G,1);
        end
        % init zonotope
        Ztest = zonotope(zeros(n,1),G);
        
        % scale zonotope to unit box (useful?)
%         shrinkFactors = 1 ./ sum(generators(box(Ztest)),2);
%         Ztest = enlarge(Ztest,shrinkFactors);

        % use generateRandom function
%         Ztest = zonotope.generateRandom(n,zeros(n,1),gamma,'exp');

        % compute Hausdorff distances
        dH_exact(i,j) = dH2box(Ztest,'exact');
        dH_naive(i,j) = dH2box(Ztest,'naive');
        dH_wgreedy(i,j) = dH2box(Ztest,'wgreedy');
        dH_wopt(i,j) = dH2box(Ztest,'wopt');
        dH_ell(i,j) = dH2box(Ztest,'ell');
        
        try
            Iinner = innerApprox(Ztest,'interval');
            dH_innerApprox(i,j) = vecnorm(rad(interval(Ztest)) - rad(Iinner));
        catch
            dH_innerApprox(i,j) = dH_naive(i,j);
        end
    end
    
end


% visualization of results
if length(dims) <= 3
    rows = 1; cols = length(dims);
elseif length(dims) <= 8
    rows = 2; cols = ceil(length(dims)/2);
else
    error("Too many dims for automated plotting");
end

% plot graphs
figure; sgtitle("Comparison of Hausdorff distance over-approximations");
for i=1:length(dims)
    % generate subplot
    subplot(rows,cols,i); hold on; box on;
    title("Dimension: " + dims(i) + ", nr. gens: " + gamma);
    
    % all graphs
    h_exact = plot(dH_exact(i,:),'g'); % lower bound
    h_naive = plot(dH_naive(i,:),'r'); % upper bound
    h_ell = plot(dH_ell(i,:),'k');
    h_wgreedy = plot(dH_wgreedy(i,:),'b');
    h_wopt = plot(dH_wopt(i,:),'m');
    
    % legend
    legend([h_exact,h_naive,h_ell,h_wgreedy,h_wopt],...
        'exact','naive','ell','wgreedy','wopt');
end


% tests finished
res = true;

end

%------------- END OF CODE --------------