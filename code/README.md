# Preliminary Version of Set-Based Observers Before Integration into CORA 2021

This is a preliminary version of the implementation of set-based observers in CORA. The final implementation will be publicly available as part of the CORA 2021 release. The current version of CORA can be found here: cora.in.tum.de. All observers for discrete-time linear systems can be found under

CORA/contDynamics/@linearSysDT

## Files

The main file for running any observer is "observe.m" All other files can be found in CORA/contDynamics/@linearSysDT/private.

## Installation

In order to run the code, please add all files to the MATLAB path and install

1. YALMIP - avialiable at: https://yalmip.github.io/
2. MOSEK Solver - availiable at: https://www.mosek.com/
3. PENLAB - available at: http://web.mat.bham.ac.uk/kocvara/penlab/

## Running the Code

To produce the results of the table in the paper, please run

test_linearSysDT_observe_01_vehicle

located under CORA/unitTests/contDynamics/linearSysDT.
