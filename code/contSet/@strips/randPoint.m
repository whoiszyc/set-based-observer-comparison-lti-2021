function p = randPoint(obj,varargin)
% randPoint - generates a random point within strips
%
% Syntax:  
%    p = randPoint(obj)
%    p = randPoint(obj,N)
%    p = randPoint(obj,N,type)
%    p = randPoint(obj,'all','extreme')
%
% Inputs:
%    obj - strips object
%    N - number of random points
%    type - type of the random point ('extreme' or 'normal')
%
% Outputs:
%    p - random point in R^n
%
% Example: 
%    S = strips([-2 1],1,1);
%
%    points = randPoint(S,100);
%
%    figure; hold on;
%    plot(points(1,:),points(2,:),'.k','MarkerSize',10);
%    plot(S,[1,2],'r');
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: zonotope/randPoint

% Author:       Matthias Althoff
% Written:      08-Mar-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% convert to polytope
P = mptPolytope(obj);

% obtain random point
p = randPoint(P,varargin{1:end});

%------------- END OF CODE --------------