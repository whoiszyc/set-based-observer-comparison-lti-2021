function E = enclosePoints(points,method)
% enclosePoints - enclose a point cloud with an ellipsoid
%
% Syntax:  
%    E = enclosePoints(points)
%    E = enclosePoints(points,method);
%
% Inputs:
%    points - matrix storing point cloud (dimension: [n,p] for p points)
%    method - method to compute the enclosing ellipsoid
%
% Outputs:
%    E - ellipsoid object
%
% Example: 
%    mu = [2 3];
%    sigma = [1 1.5; 1.5 3];
%    points = mvnrnd(mu,sigma,100)';
%
%    E1 = ellipsoid.enclosePoints(points);
%    E2 = ellipsoid.enclosePoints(points,'min-vol');
%    
%    figure; hold on
%    plot(points(1,:),points(2,:),'.k');
%    plot(E1,[1,2],'r');
%    plot(E2,[1,2],'b');
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: zonotope/enclosePoints, interval/enclosePoints

% Author: Niklas Kochdumper, Victor Gassmann
% Written: 05-May-2020
% Last update: 15-March-2021
% Last revision: ---

%------------- BEGIN CODE --------------
if ~exist('method','var')
    method = 'cov';
end

% handle degenerate case
n_nd = rank(points);
n = size(points,1);
T = eye(n);
if n_nd<n
    [T,~,~] = svd(points);
    % essentially, this is pca; since we know that the data is
    % "degenerate", the last row has to be zero, as the variance is 0
    points = T'*points;
    % remove zeros 
    points(n_nd+1:end,:) = [];
end

if strcmp(method,'cov')
    % compute the arithmetic mean of the points
    m = mean(points,2);

    % obtain sampling matrix
    translation = m*ones(1,length(points(1,:)));
    sampleMatrix = points-translation;

    % compute the covariance matrix
    C = cov(sampleMatrix');

    % singular value decomposition
    [U,~,~] = svd(C);
    
    % required ellipsoid radius in transformed space
    orientedMatrix=U'*sampleMatrix;
    m1 = max(orientedMatrix,[],2);
    m2 = min(orientedMatrix,[],2);

    nt = max([m1,m2],[],2);
    
    % enclosing ellipsoid in the transformed space
    Q = diag(nt.^2);
    
    maxDist = max(sum(diag(1./nt.^2)*orientedMatrix.^2,1));
    Q = Q * maxDist;
    
    E = ellipsoid(Q);
    
    % transform back to original space
    E = U*E  + m;
    
elseif strcmp(method,'min-vol')
    nt = size(points,1);
    Q = sdpvar(nt);
    c = mean(points,2);
    X = points-c;
    F = cone([ones(size(X,2),1),X'*Q]');
    options = sdpsettings;
    options.verbose = 0;
    % if sdpt3 is installed, use it
    if exist('sdpt3','file')
        options.solver = 'sdpt3';
    end
    %solve optimization problem
    diagnostics = optimize(F, -logdet(Q), options);
    if diagnostics.problem ~= 0
        error('Problem solving SDP!');
    end
    E = ellipsoid(inv(value(Q)^2),c);
else
    error('Method not supported!');
end

% backtransform
if n_nd<n
    E = T*ellipsoid([E.Q,zeros(n_nd,n-n_nd);zeros(n-n_nd,n)],[E.q;zeros(n-n_nd)]);
end
%------------- END OF CODE --------------