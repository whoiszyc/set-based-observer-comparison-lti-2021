function E = ellipsoid(obj,mode)
% ellipsoid - Overapproximates a mptPolytope by an ellipsoid
%
% Syntax:  
%    E = ellipsoid(obj,comptype)
%
% Inputs:
%    Z       - zonotope object
%    mode    - (Optional) 'i' (inner approx); 'o' (outer approx)
%
% Outputs:
%    E - ellipsoid object
%
% Example: 
%    Z = zonotope(rand(2,5));
%    E = ellipsoid(Z);%same as ellipsoid(Z,'o:norm:bnd')
%    plot(Z);
%    hold on
%    plot(E);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Gassmann
% Written:      15-March-2021
% Last update:   ---
% Last revision: ---

%------------- BEGIN CODE --------------
if ~exist('mode','var')
    mode = 'o';
end

if strcmp(mode,'o')
    V = vertices(obj);
    E = ellipsoid.enclosePoints(V);
else
    % first, check whether obj is degenerate
    V = vertices(obj);
    n = size(V,1);
    nt = rank(V);
    Q = eye(n);
    if nt<n
        [Q,~] = qr(V);
        V = Q'*V;
        % remove zeros if degenerate
        V(nt+1:end,:) = [];
        obj = mptPolytope(V');
    end
    A = obj.P.A;
    b = obj.P.b;
    c = sdpvar(nt,1);
    B = sdpvar(nt);
    F = cone([b-A*c,A*B]');
    options = sdpsettings;
    options.verbose = 0;
    if exist('sdpt3','file')
        options.solver = 'sdpt3';
    end
    %solve optimization problem
    diagnostics = optimize(F, -logdet(B), options);
    if diagnostics.problem ~= 0
        error('Problem with optimization...');
    end
    E = ellipsoid(value(B)^2, value(c));
    if nt<n
        E = Q*ellipsoid([E.Q,zeros(nt,n-nt);zeros(n-nt,n)],[E.q;zeros(n-nt)]);
    end
end
%------------- END OF CODE --------------