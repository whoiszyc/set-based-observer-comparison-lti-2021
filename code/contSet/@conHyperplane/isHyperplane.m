function res = isHyperplane(hyp)
% isequal - returns whether the conHyperplane object is representable as a
% simple hyperplane
%
% Syntax:  
%    res = isHyperplane(hyp)
%
% Inputs:
%    hyp - conHyperplane object
%
% Outputs:
%    res - boolean indicating whether hyp is a simple hyperplane or not
%
% Example: 
%    ---
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Victor Gassmann
% Written:      16-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
if isempty(hyp.C) || (all(all(hyp.C==0)) && all(hyp.d>=0))
    res = true;
    return;
end

% check if C is unbounded for all x on the hyperplane by checking if
% support function is bounded
c = hyp.h.c/norm(hyp.h.c);
if supportFunc(hyp,c,'upper')==inf && supportFunc(hyp,c,'lower')==-inf
    res = true;
else
    res = false;
end
%------------- END OF CODE --------------